var canvas = document.querySelector("#canvas");
var context = canvas.getContext("2d");

var xSpeed = 0;
var ySpeed = 0;
var xPos = 0;
var yPos = 0;

function keyDown(e){
  //alert(e.keyCode);
  console.log("Down "+e.keyCode);
  switch(e.keyCode){
    case 37:
      xSpeed = -5; break;
    case 38:
      ySpeed = -5; break;
    case 39:
      xSpeed = +5; break;
    case 40:
      ySpeed = +5; break;
  }
}

function keyUp(e){
  //alert(e.keyCode);
  console.log("Up "+e.keyCode);
  switch(e.keyCode){
    case 37:
      xSpeed = 0; break;
    case 38:
      ySpeed = 0; break;
    case 39:
      xSpeed = 0; break;
    case 40:
      ySpeed = 0; break;
  }
}

document.onkeydown = keyDown;
document.onkeyup = keyUp;
window.addEventListener('resize', resizeCanvas, false);
resizeCanvas();
var id = setInterval(screenReflash, 10);

function resizeCanvas() {
  canvas.width = canvas.clientWidth;
  canvas.height = canvas.clientHeight;
  screenReflash();
}

window.addEventListener("blur", function() {
  console.log("Focus #1");});

window.addEventListener("focus", function() {
  console.log("Focus #2");});


function screenReflash(){
  xPos += xSpeed;
  yPos += ySpeed;

  if(xPos<0) xPos = 0;
  if (xPos > canvas.clientWidth) xPos = canvas.clientWidth;
  if(yPos<0) yPos = 0;
  if (yPos > canvas.clientHeight) yPos = canvas.clientHeight;

  //console.log(xPos+":"+yPos+", "+xSpeed+":"+ySpeed);
  canvas.width = canvas.width;
  context.rect(xPos, yPos, 50, 50);
  context.fillStyle="#FFFFFF";
  context.fillRect(xPos,yPos,50,50);
  context.stroke();
}
